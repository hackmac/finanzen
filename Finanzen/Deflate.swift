/*
 * DeflateSwift (deflate.swift)
 *
 * Copyright (C) 2015 ONcast, LLC. All Rights Reserved.
 * Created by Josh Baker (joshbaker77@gmail.com)
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 *
 */

import Foundation
import zlib

class ZStream {
    
    fileprivate static var c_version = zlibVersion()
    fileprivate (set) static var version = String(format: "%s", locale: nil, c_version!)
    
    fileprivate func makeError(_ res: Int32) -> NSError {
        var err = ""
        switch res {
        //case Z_OK: return nil
        case Z_STREAM_END: err = "stream end"
        case Z_NEED_DICT: err = "need dict"
        case Z_ERRNO: err = "errno"
        case Z_STREAM_ERROR: err = "stream error"
        case Z_DATA_ERROR: err = "data error"
        case Z_MEM_ERROR: err = "mem error"
        case Z_BUF_ERROR: err = "buf error"
        case Z_VERSION_ERROR: err = "version error"
        default: err = "undefined error"
        }
        return NSError(domain: "deflateswift", code: Int(res), userInfo: [NSLocalizedDescriptionKey: err])
    }
    
    fileprivate var strm = z_stream()
    fileprivate var deflater = true
    fileprivate var initd = false
    fileprivate var init2 = false
    fileprivate var level = CInt(-1)
    fileprivate var windowBits = CInt(15)
    fileprivate var scratch = [UInt8](repeating: 0, count: 5000)

    init() { }

    func write(_ bytes: [UInt8], flush: Bool) throws -> [UInt8] {
        if !initd {
            var res: Int32
            if deflater {
                if init2 {
                    res = deflateInit2_(&strm, level, 8, windowBits, 8, 0, ZStream.c_version, CInt(MemoryLayout<z_stream>.size))
                } else {
                    res = deflateInit_(&strm, level, ZStream.c_version, CInt(MemoryLayout<z_stream>.size))
                }
            } else {
                if init2 {
                    res = inflateInit2_(&strm, windowBits, ZStream.c_version, CInt(MemoryLayout<z_stream>.size))
                } else {
                    res = inflateInit_(&strm, ZStream.c_version, CInt(MemoryLayout<z_stream>.size))
                }
            }
            guard res == Z_OK else {
                throw makeError(res)
            }
            initd = true
        }

        var mutBytes = bytes

        let result = try mutBytes.withUnsafeMutableBufferPointer { inBuffer in
            strm.avail_in = CUnsignedInt(inBuffer.count)
            strm.next_in = inBuffer.baseAddress

            return try scratch.withUnsafeMutableBufferPointer({ scratchBuffer in
                var res: Int32
                var outBuffer = [UInt8]()
                repeat {
                    strm.avail_out = CUnsignedInt(scratchBuffer.count)
                    strm.next_out = scratchBuffer.baseAddress

                    res = (deflater ? deflate : inflate)(&strm, flush ? 1 : 0)
                    guard res == Z_OK else {
                        throw makeError(res)
                    }
                    let have = scratchBuffer.count - Int(strm.avail_out)
                    if have > 0 {
                        outBuffer += Array(scratchBuffer[0 ..< have])
                    }
                } while (strm.avail_out == 0 && res != Z_STREAM_END)
                return outBuffer
            })
        }
        guard strm.avail_in == 0 else {
            throw makeError(-9999)
        }
        return result
    }

    deinit {
        guard initd else { return }

        if deflater {
            _ = deflateEnd(&strm)
        } else {
            _ = inflateEnd(&strm)
        }
    }
}

class DeflateStream : ZStream {
    convenience init(level : Int) {
        self.init()
        self.level = CInt(level)
    }

    convenience init(windowBits: Int) {
        self.init()
        self.init2 = true
        self.windowBits = CInt(windowBits)
    }

    convenience init(level : Int, windowBits: Int) {
        self.init()
        self.init2 = true
        self.level = CInt(level)
        self.windowBits = CInt(windowBits)
    }
}

class InflateStream : ZStream {
    override init() {
        super.init()
        deflater = false
    }

    convenience init(windowBits: Int) {
        self.init()
        self.init2 = true
        self.windowBits = CInt(windowBits)
    }
}

