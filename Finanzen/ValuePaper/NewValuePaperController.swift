//
//  NewAsset.swift
//  Finanzen
//
//  Created by henrik on 30.01.21.
//

import Cocoa

class NewValuePaperController: NSWindowController {

    @objc dynamic private var busy: Bool = false

    @objc dynamic private var useFilter: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilter) }
    }
    @objc dynamic private var useFilterAktie: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilterAktie) }
    }
    @objc dynamic private var useFilterFonds: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilterFonds) }
    }
    @objc dynamic private var useFilterDerivates: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilterDerivates) }
    }
    @objc dynamic private var useFilterIndizes: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilterIndizes) }
    }
    @objc dynamic private var useFilterCommodities: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilterCommodities) }
    }
    @objc dynamic private var useFilterAndere: Bool = false {
        didSet { updatePopUpButton(forceUpdate: oldValue != useFilterAndere) }
    }

    @objc dynamic private var searchTerm: String = "" {
        didSet {
            searchTerm = searchTerm.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            updatePopUpButton()
        }
    }
    private var lastTerm: String = ""

    @objc dynamic private var choosenValuePaper: Dictionary<String, Any>? = nil

    @IBOutlet private weak var suggestionsPopUpButton: NSPopUpButton!


    override var windowNibName: String! {
        return "NewValuePaper"
    }


    init() {
        super.init(window: nil)
    }


    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }


    override func windowDidLoad() {
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        suggestionsPopUpButton.removeAllItems()
        suggestionsPopUpButton.selectItem(at: -1)
    }


    func createValuePaperWithHistoricalQuotes() -> Any {
        let response = NSApp.runModal(for: window!)
        window?.orderOut(self)

        if NSApplication.ModalResponse.OK != response {
            return NSNull()
        }

        let selectedItem = suggestionsPopUpButton.selectedItem?.representedObject as? [String : Any]
        let entityId = selectedItem?["ISIN"] as! String
        ConsorsAPI.fetchId(stockId: entityId, marketId: "GAT") {
            if (nil != $1) {
                return
            }
            ConsorsAPI.fetchHistoricQuotes(entityId: $0!, page: 0) { (quotes, error) in
                print("Quotes zu \(entityId): \(quotes!)")
                //let metaData = quotes?["meta"] as! Dictionary<String, Any>
                //quotes["xxx"]
            }
        }

        return NSNull()
    }


    func updatePopUpButton(forceUpdate: Bool = false) {
        if (!forceUpdate && lastTerm == searchTerm) {
            return
        }
        lastTerm = searchTerm

        busy = true
        choosenValuePaper = nil
        suggestionsPopUpButton.removeAllItems()
        suggestionsPopUpButton.selectItem(at: -1)

        //YahooAPI.fetchAutocomplete(value: searchTerm, completionHandler: callbackYahoo)
        ConsorsAPI.fetchAutocomplete(value: searchTerm, completionHandler: callbackConsors)
/*
        var assetClasses: [BoerseGoAPI.AssetClass] = [];
        if useFilter {
            if useFilterAktie {
                assetClasses.append(.Stocks)
            }
            if useFilterFonds {
                assetClasses.append(.Fonds)
            }
            if useFilterIndizes {
                assetClasses.append(.Indices)
            }
            if useFilterDerivates {
                assetClasses.append(.Derivates)
            }
            if useFilterCommodities {
                assetClasses.append(.Commodities)
            }
            if useFilterAndere {
                assetClasses.append(contentsOf: [.Currency, .Futures, .Zinsen, .unknown])
            }
        }
        BoerseGoAPI.fetchAutocomplete(value: searchTerm, allowedAssetClasses: assetClasses, completionHandler: callbackBoerseGo)
 */
        //AlphaVantageAPI.fetchAutocomplete(value: searchTerm, completionHandler: callbackAlphaVantage)
/*
        var assetClasses: [OnVistaAPI.AssetClass] = [.Stocks, .Fonds, .ETF];
        if useFilter {
            if useFilterAktie {
                assetClasses.append(.Stocks)
            }
            if useFilterFonds {
                assetClasses.append(contentsOf: [.Fonds, .ETF])
            }
            if useFilterIndizes {
                assetClasses.append(.Indices)
            }
            if useFilterDerivates {
                assetClasses.append(.Derivates)
            }
            if useFilterCommodities {
                assetClasses.append(contentsOf: [.Commodities, .PreciousMetal])
            }
            if useFilterAndere {
                assetClasses.append(contentsOf: [.Currency, .Crypto, .Futures, .Anleihen])
            }
        }
        OnVistaAPI.fetchAutocomplete(value: searchTerm, allowedAssetClasses: assetClasses, completionHandler: callbackOnVista)
 */
    }


    func callbackOnVista(matches: Array<Dictionary<String, Any>>?, error: Error?) -> Void {
        if (nil == matches) || (nil != error) {
            DispatchQueue.main.async {
                self.busy = false
            }
            return
        }

        let rawMenuItems: Dictionary<String, Any> = matches!
            .reduce(into: Dictionary<String, Dictionary<String, Any>>()) { (result, match) in
                let assetClass = match["displayType"]!
                let longname = match["name"]!
                let isin = match["isin"] ?? NSNull()
                let wkn = match["wkn"] ?? match["symbol"]!
                if isin is NSNull {
                    result["[\(assetClass)] \(longname) (\(wkn))"] = match
                    return
                }
                if wkn is NSNull {
                    result["[\(assetClass)] \(longname) (\(isin))"] = match
                    return
                }
                result["[\(assetClass)] \(longname) (\(isin), \(wkn))"] = match
            }

        if (0 < rawMenuItems.count) {
            DispatchQueue.main.async {
                self.suggestionsPopUpButton.addItems(withTitles: rawMenuItems.keys.sorted())
                self.suggestionsPopUpButton.itemArray.forEach { (menuItem: NSMenuItem) in
                    menuItem.representedObject = rawMenuItems[menuItem.title]
                }
                self.suggestionsPopUpButton.selectItem(at: 0)
                self.choosenValuePaper = self.suggestionsPopUpButton.selectedItem!.representedObject as? Dictionary<String, Any>
            }
        }

        DispatchQueue.main.async {
            self.busy = false
        }
    }


    func callbackAlphaVantage(quotes: Array<Dictionary<String, Any>>?, error: Error?) -> Void {
        if (nil == quotes) || (nil != error) {
            DispatchQueue.main.async {
                self.busy = false
            }
            return
        }

        let rawMenuItems: Dictionary<String, Any> = quotes!
            .reduce(into: Dictionary<String, Dictionary<String, Any>>()) { (result, match) in
                let assetClass = match["3. type"]!
                let longName = match["2. name"]!
                let region = match["4. region"]!
                result["[\(assetClass)] \(longName) (\(region))"] = match
            }

        if (0 < rawMenuItems.count) {
            DispatchQueue.main.async {
                self.suggestionsPopUpButton.addItems(withTitles: rawMenuItems.keys.sorted())
                self.suggestionsPopUpButton.itemArray.forEach { (menuItem: NSMenuItem) in
                    menuItem.representedObject = rawMenuItems[menuItem.title]
                }
                self.suggestionsPopUpButton.selectItem(at: 0)
                self.choosenValuePaper = self.suggestionsPopUpButton.selectedItem!.representedObject as? Dictionary<String, Any>
            }
        }

        DispatchQueue.main.async {
            self.busy = false
        }
    }


    func callbackBoerseGo(quotes: Array<Dictionary<String, Any>>?, error: Error?) -> Void {
        if (nil == quotes) || (nil != error) {
            DispatchQueue.main.async {
                self.busy = false
            }
            return
        }

        let rawMenuItems: Dictionary<String, Any> = quotes!
            .filter({ (quote) -> Bool in
                let identifiers = quote["identifiers"] as! Dictionary<String, Any>
                let isin = identifiers["isin"]
                let wkn = identifiers["wkn"]
                return !(isin is NSNull && wkn is NSNull)
            })
            .reduce(into: Dictionary<String, Dictionary<String, Any>>()) { (result, quote) in
                let assetClass = (quote["assetClass"] as! Dictionary<String, Any>)["name"] ?? "???"
                let longname = quote["fullname"] ?? quote["name"]!
                let identifiers = quote["identifiers"] as! Dictionary<String, Any>
                let isin = identifiers["isin"]!
                let wkn = identifiers["wkn"]!
                if isin is NSNull {
                    result["[\(assetClass)] \(longname) (\(wkn))"] = quote
                    return
                }
                if wkn is NSNull {
                    result["[\(assetClass)] \(longname) (\(isin))"] = quote
                    return
                }
                result["[\(assetClass)] \(longname) (\(isin), \(wkn))"] = quote
            }

        if (0 < rawMenuItems.count) {
            DispatchQueue.main.async {
                self.suggestionsPopUpButton.addItems(withTitles: rawMenuItems.keys.sorted())
                self.suggestionsPopUpButton.itemArray.forEach { (menuItem: NSMenuItem) in
                    menuItem.representedObject = rawMenuItems[menuItem.title]
                }
                self.suggestionsPopUpButton.selectItem(at: 0)
                self.choosenValuePaper = self.suggestionsPopUpButton.selectedItem!.representedObject as? Dictionary<String, Any>
            }
        }
        DispatchQueue.main.async {
            self.busy = false
        }
    }


    func callbackConsors(quotes: Array<Dictionary<String, Any>>?, error: Error?) -> Void {
        if (nil == quotes) || (nil != error) {
            DispatchQueue.main.async {
                self.busy = false
            }
            return
        }

        // TODO: Put this mapper into an Adapter, so different structures of quotes can be converted into one DTO with an common interface.
        let rawMenuItems: Dictionary<String, Any> = quotes!.reduce(into: Dictionary<String, Dictionary<String, String>>()) { (result, quote) in
            let wkn = quote["WKN"]!
            let longname = quote["NAME_SHORT"]!
            result["\(longname) (\(wkn))"] = quote
        }

        if (0 < rawMenuItems.count) {
            DispatchQueue.main.async {
                self.suggestionsPopUpButton.addItems(withTitles: rawMenuItems.keys.sorted())
                self.suggestionsPopUpButton.itemArray.forEach { menuItem in
                    let item = rawMenuItems[menuItem.title] as! [String:String]
                    menuItem.representedObject = item
                    // menuItem.toolTip = "\(item["NAME_OFFICIAL"]!), ISIN:\(item["ISIN"]!)"
                }
                self.suggestionsPopUpButton.selectItem(at: 0)
                self.choosenValuePaper = self.suggestionsPopUpButton.selectedItem!.representedObject as? Dictionary<String, Any>
            }
        }
        DispatchQueue.main.async {
            self.busy = false
        }
    }


    func callbackYahoo(quotes: Array<Dictionary<String, Any>>?, error: Error?) -> Void {
        if (nil == quotes) || (nil != error) {
            DispatchQueue.main.async {
                self.busy = false
            }
            return
        }

        let rawMenuItems: Dictionary<String, Any> = quotes!.reduce(into: Dictionary<String, Dictionary<String, Any>>()) { (result, quote) in
            let typeDips = quote["typeDisp"] ?? "???"
            let longname = quote["longname"] ?? quote["shortname"]!
            let exchange = quote["exchange"] ?? "???"
            result["[\(typeDips)] \(longname) (\(exchange))"] = quote
        }
        if (0 < rawMenuItems.count) {
            DispatchQueue.main.async {
                self.suggestionsPopUpButton.addItems(withTitles: rawMenuItems.keys.sorted())
                self.suggestionsPopUpButton.itemArray.forEach { (menuItem: NSMenuItem) in
                    menuItem.representedObject = rawMenuItems[menuItem.title]
                }
                self.suggestionsPopUpButton.selectItem(at: 0)
                self.choosenValuePaper = self.suggestionsPopUpButton.selectedItem!.representedObject as? Dictionary<String, Any>
            }
        }
        DispatchQueue.main.async {
            self.busy = false
        }
    }


    @IBAction func performSelectValuePaperAction(_ sender: Any?) {
        if (nil == sender) || !(sender is NSPopUpButton) {
            return
        }
        choosenValuePaper = (sender as! NSPopUpButton).selectedItem?.representedObject as? Dictionary<String, Any>
        print(choosenValuePaper as Any)
    }


    @IBAction func performOkAction(_ sender: Any?) {
        NSApp.stopModal(withCode: NSApplication.ModalResponse.OK)
    }


    @IBAction func performCancelAction(_ sender: Any?) {
        NSApp.stopModal(withCode: NSApplication.ModalResponse.cancel)
    }

}
