//
//  AlphaVantageAPI.swift
//  Finanzen
//
//  Created by henrik on 15.05.21.
//

import Foundation

/**
 Documentation for this API: https://www.alphavantage.co/documentation/
 */
class AlphaVantageAPI: NSObject {

    enum APIFunction: String {
        case SymbolSearch = "SYMBOL_SEARCH"
        case TimeSeriesDaily = "TIME_SERIES_DAILY"
        case GlobalQuote = "GLOBAL_QUOTE"
    }

    enum AssetClass: String {
        case Stocks = "Equity"
        case Fonds = "Mutual Fund"
        case ETF = "ETF"
    }

    private static let API_KEY = "D63BGVDJ72EYM3JC"

    private static let BASE_URL = "https://www.alphavantage.co/query"


    // MARK: -


    private class func createRequest(parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: BASE_URL)!
        urlComponents.queryItems = queryItems
        urlComponents.queryItems?.append(URLQueryItem(name: "apikey", value: API_KEY))
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        //request.allHTTPHeaderFields = headers

        return request
    }


    class func fetchAutocomplete(value: String!, completionHandler handler: @escaping (Array<Dictionary<String, Any>>?, Error?) -> Void) -> Void {
        let parameter = [
            /* Function to call */
            URLQueryItem(name: "function", value: APIFunction.SymbolSearch.rawValue),
            /* Name to search for */
            URLQueryItem(name: "keywords", value: value)
            //URLQueryItem(name: "datatype", value: "csv")    // "json" (default) and "csv"
        ]

        let request = createRequest(parameter: parameter)
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject["bestMatches"] as? Array<Dictionary<String, Any>>, error)
            } else {
                print(httpResponse)
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }


    class func fetchHistoricQuotes(symbol: String, handler: @escaping (Dictionary<String, Any>?, Error?) -> Void) -> Void {
        let request = createRequest(parameter: [
            /* Function to call */
            URLQueryItem(name: "function", value: APIFunction.TimeSeriesDaily.rawValue),
            /* Symbol name to get the daily quotes */
            URLQueryItem(name: "symbol", value: symbol),
            URLQueryItem(name: "outputsize", value: "full"),  // "compact" (default, 100 data sets) and "full" (last 20+ years)
            //URLQueryItem(name: "datatype", value: "csv")    // "json" (default) and "csv"
        ])
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject, error)
            } else {
                print(httpResponse)
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }

}
