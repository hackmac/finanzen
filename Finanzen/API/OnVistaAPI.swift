//
//  OnVistaAPI.swift
//  Finanzen
//
//  Created by henrik on 15.05.21.
//

import Foundation


class OnVistaAPI: NSObject {

    /** Got directly from the web page, don't know any api to get the values from. */
    static let MarketPlace = [
        250390546: "Stuttgart (EUR, Echtzeit)",
        247852218: "Gettex (EUR, Echtzeit)",
        251968371: "Swiss Exchange (EUR, verzögert)",
        237149521: "Tradegate (EUR, Echtzeit)",
        247757602: "Frankfurt (EUR, verzögert)",
        247946261: "Baader Bank (EUR, Echtzeit)",
        247757601: "Berlin (EUR, Echtzeit)",
        244980258: "Hamburg (EUR, Echtzeit)",
        234261621: "KVG (EUR, Echtzeit)",
        237149522: "München (EUR, Echtzeit)",

        30820934: "Xetra",
        30920189: "Tradegate",
        30888375: "Stuttgart",
        134129224: "Quotrix",
        244494501: "LS Exchange",
        1437803: "Nasdaq OTC",
        120495749: "Gettex",
        30888377: "Frankfurt",
        30922370: "Lang & Schwarz",
        30888374: "München",
        30888373: "Düsseldorf",
        30888378: "Hamburg",
        54055261: "London Trade Rep.",
        30888376: "Berlin",
        30888379: "Hannover",
        46985092: "Baader Bank",
        116588499: "Swiss Exchange",
        132936070: "Swiss Exchange"
    ]


    private static let ENDPOINT_BASE_URL = "https://api.onvista.de/api/v1/instruments/"
    private static let ENDPOINT_SEARCH = "\(ENDPOINT_BASE_URL)\(ApiCommand.Query.rawValue)"
    private static let ENDPOINT_ONE_DATE = "\(ENDPOINT_BASE_URL)FUND/3569522/\(ApiCommand.TimesAndSales.rawValue)"
                + "?idNotation=234261621"
                + "&startDate=2021-05-12T00:00:00.000+00:00"
    private static let ENDPOINT_FREE_RANGE = "\(ENDPOINT_BASE_URL)FUND/3569522/\(ApiCommand.TimesAndSales.rawValue)"
                + "?idNotation=234261621"
        + "&order=\(ApiArgumentOrder.DESC.rawValue)"
                + "&startDate=2021-05-12T00:00:00.000+00:00"
                + "&endDate=2021-05-12T23:59:59.000+00:00"
    private static let ENDPOINT_EOD_HISTORY = "\(ENDPOINT_BASE_URL)FUND/3569522/\(ApiCommand.EodHistory.rawValue)"
                + "?idNotation=234261621"
        + "&order=\(ApiArgumentOrder.DESC.rawValue)"
        + "&range=\(ApiArgumentRange.M1.rawValue)"
                + "&startDate=2021-04-16"

    enum ApiCommand: String {
        case Query = "query"
        case TimesAndSales = "times_and_sales"
        case EodHistory = "eod_history"
    }

    enum ApiArgumentRange: String {
        case D1 = "D1"
        case M1 = "M1"
        case M3 = "M3"
        case M6 = "M6"
        case Y1 = "Y1"
        case Y3 = "Y3"
        case Y5 = "Y5"
    }

    enum ApiArgumentOrder: String {
        case ASC = "ASC"
        case DESC = "DESC"
    }

    enum AssetClass: String {
        case Stocks = "STOCK"
        case Indices = "INDEX"
        case Currency = "CURRENCY", Crypto = "CRYPTO"
        case Commodities = "Commoditiy", PreciousMetal = "PRECIOUS_METAL"
        case Fonds = "FUND", ETF = "ETF"
        case Anleihen = "BOND", Futures = "FUTURE", Derivates = "Derivates"
    }


    private class func createRequest(forType entityType: String, entityValue: UInt, parameter queryItems: [URLQueryItem]) -> URLRequest {
        let url = "\(ENDPOINT_BASE_URL)\(entityType)/\(entityValue)/\(ApiCommand.TimesAndSales.rawValue)"
        return createRequest(forUrl: url, parameter: queryItems)
    }


    private class func createRequest(forUrl url: String, parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: url)!
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        print("requesting url = '\(String(describing: request.url))'")
        //request.allHTTPHeaderFields = headers

        return request
    }


    class func fetchAutocomplete(value: String!, allowedAssetClasses: [AssetClass] = [.Stocks, .Fonds, .ETF],
                                 completionHandler handler: @escaping (Array<Dictionary<String, Any>>?, Error?) -> Void) -> Void {
        let parameter = [
            /* properties for paging */
            URLQueryItem(name: "limit", value: 50.description),   // default: 10
            /* the search term */
            URLQueryItem(name: "searchValue", value: value)
        ]

        let request = createRequest(forUrl: ENDPOINT_SEARCH, parameter: parameter)
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler((jsonObject["list"] as? Array<Dictionary<String, Any>>)?
                            .filter({ (match: Dictionary<String, Any>) -> Bool in
                                var entityType = match["entitySubType"]
                                if (nil == entityType) {
                                    entityType = match["entityType"]
                                }
                                return (allowedAssetClasses.first(where: { (asset: AssetClass) -> Bool in
                                    return asset.rawValue == entityType as! String
                                }) != nil)
                            }), error)
            } else {
                print(httpResponse)
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }


    class func fetchHistoricQuotes(entityType: String, entityValue: UInt, handler: @escaping (Dictionary<String, Any>?, Error?) -> Void) -> Void {
        let request = createRequest(forType: entityType, entityValue: entityValue,
                                    parameter: [
                                        URLQueryItem(name: "startDate", value: "2021-05-12T00:00:00.000+00:00"),
                                        URLQueryItem(name: "endDate", value: "2021-05-12T23:59:59.000+00:00"),
                                        URLQueryItem(name: "order", value: "DESC"),
                                        /* exchange market id */
                                        URLQueryItem(name: "idNotation", value: "300067706") // for TradeGate as market place
                                    ])
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject, error)
            } else {
                print(httpResponse)
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }

}
