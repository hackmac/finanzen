//
//  ConsorsAPI.swift
//  Finanzen
//
//  Created by henrik on 04.02.21.
//

import Cocoa
import zlib
import Compression

import Gzip


extension Data {
    /**

     */
    var deflated: Self {
        let bufferMaxSize = count * 2
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferMaxSize)
        let bufferUsedSize = withUnsafeBytes {
            compression_decode_buffer(
                buffer, bufferMaxSize,
                $0.baseAddress!.bindMemory(to: UInt8.self, capacity: count), count,
                nil,
                COMPRESSION_ZLIB
            )
        }
        let result = Data(bytes: buffer, count: bufferUsedSize)
        buffer.deallocate()
        return result
    }

    var inflated: Self {
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: count)
        let byteCount = withUnsafeBytes {
            compression_encode_buffer(
                buffer, count,
                $0.baseAddress!.bindMemory(to: UInt8.self, capacity: count), count,
                nil, COMPRESSION_ZLIB
            )
        }
        let result = Data(bytes: buffer, count: byteCount)
        buffer.deallocate()
        return result
    }

    /**
     Returns a new String with hexadecimal characters of bytes stored in it self.
     @result A new hex-string
     */
    var hexString: String {
        return self.withUnsafeBytes {
            $0.map { b in
                String(format: "%02x", b)
            }
            .joined(separator: ", ")
        }
    }

}


class ConsorsAPI: NSObject {

    private static let BASE_URL = "https://www.consorsbank.de/web-financialinfo-service/api"
    private static let ENDPOINT_AUTOCOMPLETE_ORIGINAL = "https://www.consorsbank.de/web-sec-service/api/marketdata/securities/searchproposalv1"
    private static let ENDPOINT_AUTOCOMPLETE = "\(BASE_URL)/marketdata/securities/searchproposalv1"

    // MARK: - Such-API Beispiel-URLs

    private static let suchAPI = "\(ENDPOINT_AUTOCOMPLETE)?frontendType=web&maxResults=10&searchValue=beiersdorf"
    private static let suchAPIResponse = try! JSONSerialization.jsonObject(with: """
{
    "Info":{
        "Errors":[
            {
                "ERROR_CODE":"1",
                "ERROR_MESSAGE":"Parameter: Parameter searchValue not set or invalid"
            }
        ],
        "NAME":"SearchProposalListV1"
    },
    "SearchProposalListV1":{
        "LIST":[
            {
                "ID_NOTATION":"143111",
                "ISIN":"DE0005200000",
                "NAME":"BEIERSDORF AG",
                "NAME_OFFICIAL":"BEIERSDORF AG INHABER-AKTIEN O.N.",
                "NAME_SHORT":"BEIERSDORF",
                "TOOL_TYPE":"STO",
                "WKN":"520000"
            },{
                "ID_NOTATION":"412340506",
                "ISIN":"DE000GZ975B5",
                "NAME":"OPEN END TURBO OPTIONSSCHEIN SHORT AUF BEIERSDORF AG",
                "NAME_OFFICIAL":"GOLDMAN SACHS BANK EUROPE SE TUBEAR O.END BEIERSD. 137,1031",
                "NAME_SHORT":"OPEN END TURBO OPTIONSSCHEIN SHORT AUF BEIERSDORF",
                "TOOL_TYPE":"CER",
                "WKN":"GZ975B"
            },{
                ...
            },{
                "ID_NOTATION":"405269371",
                "ISIN":"DE000PE6ZRQ0",
                "NAME":"UNLIMITED TURBO LONG AUF BEIERSDORF AG",
                "NAME_OFFICIAL":"BNP PARIBAS EM.-U.HANDELSG.MBH TURBOL O.END BEIERSD. 105,25",
                "NAME_SHORT":"UNLIMITED TURBO LONG AUF BEIERSDORF",
                "TOOL_TYPE":"LEVERAGE",
                "WKN":"PE6ZRQ"
            }
        ]
    }
}
""".data(using: .utf8)!)


    // MARK: - Exchanging chart data via web sockets using compressed JSON ("v2.ws-jsjsonc.mdgms.com")

    // using web sockets in Swift see: https://github.com/tidwall/SwiftWebSocket
    // information for debugging original code: https://documentation.chartiq.com/

    private static let subprotocolName = "v2.ws-jsjsonc.mdgms.com"
    private static let ENDPOINT_URL_CONFIG = "\(BASE_URL)/chartlib/url"
    private static let httpBaseEndpoint = "https://charts-interactive.consorsbank.de"   // the host value from the URL-config above
    private static let wssBaseEndpoint = "wss://charts-interactive.consorsbank.de:443/ws"

    private static let endpointTimeSeriesEODList = "\(httpBaseEndpoint)/api/v1/vendor/chartIQ/timeSeries/eod/list"
    private static let endpointTimeSeriesIntradayList = "\(httpBaseEndpoint)/api/v1/vendor/chartIQ/timeSeries/intraday/subsample/list"

    private static let authenticationByTokenRequestCompressed = Data([120,156,116,81,239,79,219,48,16,253,87,44,127,26,82,213,216,33,41,52,124,89,232,84,74,81,41,98,8,49,38,20,185,206,37,245,234,196,204,63,82,40,234,255,190,75,249,0,211,52,203,31,124,239,238,222,123,231,123,163,11,112,78,212,64,51,58,53,161,45,133,87,166,205,178,60,248,53,180,94,201,67,124,254,122,103,54,208,222,194,239,0,206,211,1,189,7,235,16,167,25,31,80,223,167,104,246,70,59,161,3,244,143,213,40,65,186,121,157,79,218,101,157,220,243,89,188,173,187,250,42,183,211,169,226,143,99,119,115,57,190,222,61,231,249,143,124,50,127,124,168,111,150,113,114,117,114,60,143,88,37,236,249,69,103,210,176,110,146,133,141,245,108,54,250,181,28,69,115,57,251,182,73,232,126,63,160,206,84,126,43,108,111,119,97,118,74,107,17,165,67,70,190,44,132,84,173,55,110,125,70,46,91,15,154,32,64,150,223,201,3,225,108,200,211,51,98,187,140,179,241,144,29,145,11,144,27,19,197,140,51,188,156,76,149,133,202,188,68,156,191,19,149,117,60,209,10,103,255,42,131,243,166,33,171,160,116,121,132,67,27,215,171,10,121,16,192,184,2,225,131,133,162,210,162,118,197,86,32,92,126,250,7,134,118,27,241,162,154,208,20,170,212,80,160,65,176,152,164,89,146,178,195,249,40,176,32,65,117,98,133,101,205,251,66,10,167,118,200,194,89,114,154,158,140,80,173,87,65,214,1,149,66,174,161,16,127,109,168,112,66,251,79,226,63,159,246,255,41,132,86,218,215,103,180,90,56,144,22,14,77,255,98,216,191,255,3,0,0,255,255])
    private static let authenticationByTokenRequestOriginal = """
{"Message":"Foundation::AuthenticationByTokenRequest","Version":1,"token":{"value":{"b64":"JgACnOg4V1H2wgvgKArFFi1Z9sPI9NzpAAYACJZXgPO24K73J/0farBGvo5uhm4Mr2lHH6jO6/JcHDk4"}},"software":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/115.0 (Mdg2Client@custom build)","os":"MacIntel","feature_flags_wanted":{"value":0},"maximum_idle_interval":45000000,"maximum_receivable_message_size":1048576,"flags":0,"cache_authentication_salt":{"value":[]},"cache_authentication_encrypted_secret":{"encrypted_secret":[]}}
"""
    private static let authenticationByTokenRequestPretty = """
{
    "Message":"Foundation::AuthenticationByTokenRequest",
    "Version":1,
    "token":{
        "value":{
            "b64":"JgACnOg4V1H2wgvgKArFFi1Z9sPI9NzpAAYACJZXgPO24K73J/0farBGvo5uhm4Mr2lHH6jO6/JcHDk4"
        }
    },
    "software":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/115.0 (Mdg2Client@custom build)",
    "os":"MacIntel",
    "feature_flags_wanted":{
        "value":0
    },
    "maximum_idle_interval":45000000,
    "maximum_receivable_message_size":1048576,
    "flags":0,
    "cache_authentication_salt":{
        "value":[]
    },
    "cache_authentication_encrypted_secret":{
        "encrypted_secret":[]
    }
}
"""

    /**
     POST body:
     {
        value: {
            data: {
                type: "trade",
                quality: "BST",
                granularity: undefined,
                id: "9386081",  // an id for Beiersdorf
                range: {
                    start: "2003-07-14T00:00:00.000Z",
                    end: "2008-07-14T00:00:00.000Z"
                }
            },
            meta: {
                pagination: {
                    limit: 5000
                }
            }
        }
     }
     
     */


    // MARK: - Suchergebnisse Beispiel-URLs

    /**
     Possible fields are "ExchangesV2", "HistoryV1", "BasicV1" which can be used alternativly.
     Also known: "TimesSalesV1", "QuotesV1" and  others like "FigureV2", "BreakdownV2"
     */
    private static let stockAfterSearchURL = "\(BASE_URL)/funds?id=_158463&field=BasicV1&field=ExchangesV2&field=HistoryV1&resolution=1D&range=-1&page=0"



    private class func createRequest(forEndpoint endpoint: String, parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: BASE_URL)!
        urlComponents.path += "/\(endpoint)"
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        //request.allHTTPHeaderFields = headers

        return request
    }


    private class func createRequest(forUrl url: String, parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: url)!
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        //request.allHTTPHeaderFields = headers

        return request
    }
    

    /**
     Fetches auto complete suggestions by term or phrase.
     - Parameters:
        - value:    The term or phrase to look for
        - handler:  A completion handler that is called after the requested result is returned from the API.
                    This completion handler function takes two arguments and has no return.
                    The first argument is an array of dictionaries with the structure listed below.
                    The second argument is an Error object that takes any error information. If is not `nil` the first argument is undefined.
        (
            {
                "ID_NOTATION" = 143111;
                ISIN = DE0005200000;
                NAME = "BEIERSDORF AG";
                "NAME_OFFICIAL" = "BEIERSDORF AG INHABER-AKTIEN O.N.";
                "NAME_SHORT" = BEIERSDORF;
                "TOOL_TYPE" = STO;
                WKN = 520000;
            }, {
                "ID_NOTATION" = 412340506;
                ISIN = DE000GZ975B5;
                NAME = "OPEN END TURBO OPTIONSSCHEIN SHORT AUF BEIERSDORF AG";
                "NAME_OFFICIAL" = "GOLDMAN SACHS BANK EUROPE SE TUBEAR O.END BEIERSD. 137,1031";
                "NAME_SHORT" = "OPEN END TURBO OPTIONSSCHEIN SHORT AUF BEIERSDORF";
                "TOOL_TYPE" = CER;
                WKN = GZ975B;
            }, {
                ...
            }, {
                "ID_NOTATION" = 405269371;
                ISIN = DE000PE6ZRQ0;
                NAME = "UNLIMITED TURBO LONG AUF BEIERSDORF AG";
                "NAME_OFFICIAL" = "BNP PARIBAS EM.-U.HANDELSG.MBH TURBOL O.END BEIERSD. 105,25";
                "NAME_SHORT" = "UNLIMITED TURBO LONG AUF BEIERSDORF";
                "TOOL_TYPE" = LEVERAGE;
                WKN = PE6ZRQ;
            }
        );
     */
    class func fetchAutocomplete(value: String!, completionHandler handler: @escaping (Array<Dictionary<String, String>>?, Error?) -> Void) {

        // MARK - Test decompress

        /*print("original request (\(authenticationByTokenRequestOriginal.count)): \(authenticationByTokenRequestOriginal.data(using: .utf8)!.hexString)")
        print("compressed request (\(authenticationByTokenRequestCompressed.count)): \(authenticationByTokenRequestCompressed.hexString)\n")*/

        /*
        let d = NSData(data: authenticationByTokenRequestCompressed/*[2...]*/)
        print("compressed request (\(d.count)): \((d as Data).hexString)")
        do {
            let x: Data = try d.decompressed(using: .zlib) as Data
            print("decompressed request: \(String(data: x, encoding: .utf8)!)")
        }
        catch {
            print("Error: \(error)")
        }
        let x = (d as Data).deflated
        print("decompressed request (\(x.count)): \(x.hexString)")
*/
        // MARK - Test compress using COMPRESSION_ZLIB
/*
        let result = authenticationByTokenRequest.data(using: .utf8)!.inflated
        print("compressed request (\(result.count)): \(result.hexString)")
*/
        // MARK - Test compress using zlib

        // implementation example see: https://github.com/tidwall/DeflateSwift
        // TODO: cannot be used as a Package because of the swift-tools-version:4.0 - it should be version 5.x

        // compress
        do {
            let deflater = DeflateStream()
            let inflated = Array(authenticationByTokenRequestOriginal.data(using: .utf8)!)
            let deflated = try deflater.write(inflated, flush: true)

            print("deflated (\(deflated.count)):\n\(Data(deflated).hexString)")
            print("compresses (\(authenticationByTokenRequestCompressed.count)):\n\(authenticationByTokenRequestCompressed.hexString)")
            print("\nMatches to compressed request: \(deflated == Array(authenticationByTokenRequestCompressed))")
            if deflated != Array(authenticationByTokenRequestCompressed) {
                let diffIdx = Array(zip(deflated, Array(authenticationByTokenRequestCompressed)))
                    .firstIndex {
                        $0 != $1
                    }
                print("diff index: \(String(describing: diffIdx))")
            }
        }
        catch {
          print("Error: \(error)")
        }

        // decompress
        /*do {
            let inflater = InflateStream()
            let deflated = Array(authenticationByTokenRequestCompressed)
            let inflated = try inflater.write(deflated, flush: true)

            print("inflated (\(inflated.count)): \(Data(inflated).hexString)")
            print("Matches to original request: \(inflated == Array(authenticationByTokenRequestOriginal.data(using: .utf8)!))")
        }
        catch {
          print("Error: \(error)")
        }*/

        // MARK - continue normal

        let request = createRequest(forUrl: ENDPOINT_AUTOCOMPLETE,
                                    parameter: [
                                      URLQueryItem(name: "searchValue", value: value),
                                      URLQueryItem(name: "frontendType", value: "web"),
                                      URLQueryItem(name: "maxResults", value: "10")
                                    ])
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let apiProtocolName = "SearchProposalListV1"
                let jsonObject = try! JSONSerialization.jsonObject(with: data!) as! [String : [String : Any]]
                let apiName = (jsonObject["Info"] as! [String : String])["NAME"]
                if apiProtocolName == apiName {
                    let proposals = jsonObject[apiProtocolName]!["LIST"] as! [[String : String]]
                    handler(proposals, nil)
                } else {
                    let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode,
                                        userInfo: [
                                            NSLocalizedFailureReasonErrorKey : "Incorrect API version! Expected version: \(apiProtocolName)"
                                        ])
                    handler(nil, error)
                }
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode,
                                    userInfo: [
                                        NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                                    ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }


    /**
     Fetches historic quotes of the given entity.
     - Parameters:
        - entityId: The Consors internal id of the stock. You can not use WKN or ISIN. It depends on the exchange market.
        - page:     The page number. If exceeds the limits the result is empty.
        - handler:  A completion handler that is called after the requested result is returned from the API.
                    This completion handler function takes two arguments and has no return.
                    The first argument is an array of dictionaries with the structure listed below.
                    The second argument is an Error object that takes any error information. If is not `nil` the first argument is undefined.

     {
        "AMOUNT":22,
        "DATETIME_MARKET_CLOSE":"2023-06-12T15:35:00+0000",
        "DATETIME_MARKET_OPEN":"2023-06-12T07:00:00+0000",
        "ITEMS":[
            {
                "DATETIME_LAST":"2023-07-11T12:00:00+0000",
                "FIRST":146.45,
                "HIGH":147.5,
                "ISO_CURRENCY":"EUR",
                "LAST":147.25,
                "LOW":145.8,
                "TOTAL_VOLUME":195063.0
            }, {
                "DATETIME_LAST":"2023-07-10T12:00:00+0000",
                "FIRST":146.45,
                "HIGH":147.15,
                "ISO_CURRENCY":"EUR",
                "LAST":145.85,
                "LOW":145.6,
                "TOTAL_VOLUME":189520.0
            }, {
                ...
            }, {
                "DATETIME_LAST":"2023-06-12T12:00:00+0000",
                "FIRST":169.5,
                "HIGH":170.8,
                "ISO_CURRENCY":"EUR",
                "LAST":169.0,
                "LOW":168.85,
                "TOTAL_VOLUME":270946.0
            }
        ],
        "PAGE_INDEX":0,
        "PAGE_SIZE":50,
        "TOTAL_AMOUNT":22,
        "TOTAL_PAGES":1
     }
     */
    class func fetchHistoricQuotes(entityId: String, page: UInt, handler: @escaping (Dictionary<String, Any>?, Error?) -> Void) -> Void {
        let request = createRequest(forEndpoint: "funds",
                                    parameter: [
                                        URLQueryItem(name: "field", value: "HistoryV1"),
                                        URLQueryItem(name: "resolution", value: "1D"),  /* or 1W, 1M, etc */
                                        URLQueryItem(name: "range", value: "-1"),  /* count of months */
                                        URLQueryItem(name: "page", value: "\(page)"),
                                        URLQueryItem(name: "id", value: entityId)
                                    ])
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!) as! [[String : Any]]
                let quotes = jsonObject[0]["HistoryV1"] as! [String : Any]
                handler(quotes, error)
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }


    /**
     Fetches the Consors internal stock id that depends on the WKN/ISIN and the exchange market.
     - Parameters:
        - stockId:  The id of the stock, can be an ISIN or WKN
        - marketId: The id of the exchange market. "GER" for Xetra, "GAT" for TradeGate, "LUSG" for Lang & Schwarz, "@DE" for außerbörslich Deutschland, etc.
        - handler:  A completion handler that is called after the requested result is returned from the API.
                    This completion handler function takes two arguments and has no return.
                    The first argument is a string that contains the Consors internal id of the stock identifies by stockId.
                    The second argument is an Error object that takes any error information. If is not `nil` the first argument is undefined.
     */
    class func fetchId(stockId: String, marketId: String, handler: @escaping (String?, Error?) -> Void) -> Void {
        let request = createRequest(forEndpoint: "funds",
                                    parameter: [
                                        URLQueryItem(name: "field", value: "ExchangesV2"),
                                        URLQueryItem(name: "id", value: stockId)
                                    ])
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!) as! [[String : Any]]
                let exchanges = jsonObject[0]["ExchangesV2"] as! [[String : Any]]
                let id = exchanges.first {
                    $0["EXCHANGE_CODE"] as! String == marketId
                }!["CONSORS_ID"] as! String
                handler(id, error)
            } else {
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }

}
