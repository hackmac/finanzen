//
//  YahooAPI.swift
//  Finanzen
//
//  Created by henrik on 30.01.21.
//

import Cocoa

class YahooAPI: NSObject {

    /** This is another, direct URL that can be used for obtaining the actual price as JSON data. */
    static let yahooDirectPrice = "https://query1.finance.yahoo.com/v10/finance/quoteSummary/BAYN.DE?modules=price"
    /** This is another, direct URL that can be used for obtaining historical data as CSV values. */
    static let yahooDirectHistoricalData = "https://query1.finance.yahoo.com/v7/finance/download/BAYN.DE?period1=1580155207&period2=1611777607&interval=1d&events=history&includeAdjustedClose=true"

    private static let headers = [
        "x-rapidapi-key": "f1a58a7648msh6f59553fddb142ep10331fjsna484b55f5577",
        "x-rapidapi-host": "apidojo-yahoo-finance-v1.p.rapidapi.com"
    ]

    private static let BASE_URL = "https://apidojo-yahoo-finance-v1.p.rapidapi.com"
    private static let ENDPOINT_AUTOCOMPLETE = "auto-complete"
    private static let ENDPOINT_QUOTES = "market/v2/get-quotes"
    private static let ENDPOINT_HISTORICAL_DATA = "stock/v3/get-historical-data"


    private class func createRequest(forEndpoint endpoint: String, parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: BASE_URL)!
        urlComponents.path = "/" + endpoint
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        return request
    }


    /**
     Fetches auto complete suggestions by term or phrase from the Yahoo Finance API at RapidAPI.com.

     Can be used to translate WKN, ISIN or company names to symbols that Yahoo needs to use them as the identifier of a value.
     - Parameters:
        - value:    The term or phrase to look for
        - handler:  A completion handler that is called after the requested result is returned from the API.
                    This completion handler function takes two arguments and has no return.
                    The first argument is an array of dictionaries with the structure listed below.
                    The second argument is an Error object that takes any error information. If is not `nil` the first argument is undefined.

                 (
                     {
                         exchange = GER;
                         index = quotes;
                         isYahooFinance = 1;
                         longname = "Bayer Aktiengesellschaft";
                         quoteType = EQUITY;
                         score = 21063;
                         shortname = "BAYER AG NA O.N.";
                         symbol = "BAYN.DE";
                         typeDisp = Equity;
                     },
                     {
                         exchange = FRA;
                         index = quotes;
                         isYahooFinance = 1;
                         longname = "Bayer Aktiengesellschaft";
                         quoteType = EQUITY;
                         score = 20115;
                         shortname = "BAYER AG NA O.N.";
                         symbol = "BAYN.F";
                         typeDisp = Equity;
                     },
                     {
                         exchange = PNK;
                         index = quotes;
                         isYahooFinance = 1;
                         longname = "Bayerische Motoren Werke Aktiengesellschaft";
                         quoteType = EQUITY;
                         score = 20034;
                         shortname = "BAYERISCHE MOTOREN WERKE AG";
                         symbol = BMWYY;
                         typeDisp = Equity;
                     },
                     {
                         exchange = PNK;
                         index = quotes;
                         isYahooFinance = 1;
                         longname = "Bayer Aktiengesellschaft";
                         quoteType = EQUITY;
                         score = 20023;
                         shortname = "BAYER AG";
                         symbol = BAYRY;
                         typeDisp = Equity;
                     },
                     {
                         exchange = MIL;
                         index = quotes;
                         isYahooFinance = 1;
                         longname = "Bayer Aktiengesellschaft";
                         quoteType = EQUITY;
                         score = 20012;
                         shortname = BAYER;
                         symbol = "BAY.MI";
                         typeDisp = Equity;
                     },
                     {
                         exchange = GER;
                         index = quotes;
                         isYahooFinance = 1;
                         longname = "Bayer Aktiengesellschaft";
                         quoteType = EQUITY;
                         score = 20011;
                         shortname = "BAYER ADR /1/4";
                         symbol = "BAYA.DE";
                         typeDisp = Equity;
                     },
                     {
                         exchange = IOB;
                         index = quotes;
                         isYahooFinance = 1;
                         quoteType = EQUITY;
                         score = 20005;
                         shortname = "BAYER AG BAYER ORD SHS";
                         symbol = "0P6S.IL";
                         typeDisp = Equity;
                     }
                 )
     */
    class func fetchAutocomplete(value: String!, completionHandler handler: @escaping (Array<Dictionary<String, Any>>?, Error?) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: createRequest(forEndpoint: ENDPOINT_AUTOCOMPLETE,
                                                                      parameter: [
                                                                        URLQueryItem(name: "region", value: "DE"),
                                                                        URLQueryItem(name: "q", value: value)
                                                                      ])) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject["quotes"] as? Array<Dictionary<String, Any>>, error)
            }
        }
        dataTask.resume()
    }


    /**
     Fetches the quotes by `symbols` from the Yahoo Finance API at RapidAPI.com.

     You can fetch multiple quotes (like ["BAYN.DE", "IFX.DE"]), one for each symbol passed in the `symbols` array.
     - Parameters:
        - symbols:  A list of symbols that comforms to the Yahoo value identifier
        - handler:  A completion handler that is called after the requested result is returned from the API.
                    This completion handler function takes two arguments and has no return.
                    The first argument is an Array of Dictionary for each symbol with the structure listed below.
                    The second argument is an Error object that takes any error information. If is not `nil` the first argument is undefined.
     
                 (
                     {
                         ask = "51.13";
                         askSize = 21;
                         averageDailyVolume10Day = 2999221;
                         averageDailyVolume3Month = 3718707;
                         beta = "1.341164";
                         bid = "51.12";
                         bidSize = 284;
                         bookValue = "31.888";
                         components = (
                             "^GDAXI",
                             "SX4P.Z",
                             "SX5P.Z",
                             "^STOXX50E"
                         );
                         currency = EUR;
                         dividendRate = "2.8";
                         dividendYield = "5.59";
                         dividendsPerShare = "2.8";
                         earningsTimestamp = 1614216600;
                         earningsTimestampEnd = 1614216600;
                         earningsTimestampStart = 1614216600;
                         ebitda = "-2039000064";
                         epsCurrentYear = "6.91";
                         epsForward = "7.49";
                         epsTrailingTwelveMonths = "-9.557";
                         esgPopulated = 0;
                         exDividendDate = 1588118400;
                         exchange = GER;
                         exchangeDataDelayedBy = 0;
                         exchangeTimezoneName = "Europe/Berlin";
                         exchangeTimezoneShortName = CET;
                         fiftyDayAverage = "49.957726";
                         fiftyDayAverageChange = "0.99227524";
                         fiftyDayAverageChangePercent = "0.019862298";
                         fiftyTwoWeekHigh = "78.34";
                         fiftyTwoWeekHighChange = "-27.389996";
                         fiftyTwoWeekHighChangePercent = "-0.3496298";
                         fiftyTwoWeekLow = "39.91";
                         fiftyTwoWeekLowChange = "11.040001";
                         fiftyTwoWeekLowChangePercent = "0.2766224";
                         fiftyTwoWeekRange = "39.91 - 78.34";
                         firstTradeDateMilliseconds = 850723200000;
                         floatShares = 982420000;
                         forwardPE = "6.8024035";
                         fullExchangeName = XETRA;
                         gmtOffSetMilliseconds = 3600000;
                         heldPercentInsiders = 0;
                         heldPercentInstitutions = "42.749";
                         language = "de-DE";
                         longName = "Bayer Aktiengesellschaft";
                         market = "de_market";
                         marketCap = 50054504448;
                         marketState = POSTPOST;
                         messageBoardId = "finmb_103375_lang_de";
                         pegRatio = "2.07";
                         priceEpsCurrentYear = "7.373372";
                         priceHint = 2;
                         priceToBook = "1.5977798";
                         priceToSales = "1.1873919";
                         quoteSourceName = "Delayed Quote";
                         quoteSummary = {
                             earnings = {
                                 earningsChart = {
                                     currentQuarterEstimateDate = "1Q";
                                     currentQuarterEstimateYear = 2018;
                                     earningsDate = (
                                         1614211200
                                     );
                                     quarterly = (
                                         {
                                             actual = "2.62";
                                             date = "1Q2017";
                                             estimate = "2.39";
                                         },
                                         {
                                             actual = "1.81";
                                             date = "2Q2017";
                                             estimate = "1.76";
                                         },
                                         {
                                             actual = "1.47";
                                             date = "3Q2017";
                                             estimate = "1.49";
                                         },
                                         {
                                             actual = "1.41";
                                             date = "4Q2017";
                                             estimate = "1.13";
                                         }
                                     );
                                 };
                                 financialCurrency = EUR;
                                 financialsChart = {
                                     quarterly = (
                                         {
                                             date = "4Q2019";
                                             earnings = 1414000000;
                                             revenue = 10750000000;
                                         },
                                         {
                                             date = "1Q2020";
                                             earnings = 1489000000;
                                             revenue = 12845000000;
                                         },
                                         {
                                             date = "2Q2020";
                                             earnings = "-9548000000";
                                             revenue = 10054000000;
                                         },
                                         {
                                             date = "3Q2020";
                                             earnings = "-2744000000";
                                             revenue = 8506000000;
                                         }
                                     );
                                     yearly = (
                                         {
                                             date = 2016;
                                             earnings = 4531000000;
                                             revenue = 34943000000;
                                         },
                                         {
                                             date = 2017;
                                             earnings = 7336000000;
                                             revenue = 35015000000;
                                         },
                                         {
                                             date = 2018;
                                             earnings = 1695000000;
                                             revenue = 36742000000;
                                         },
                                         {
                                             date = 2019;
                                             earnings = 4091000000;
                                             revenue = 43545000000;
                                         }
                                     );
                                 };
                                 maxAge = 86400;
                             };
                         };
                         quoteType = EQUITY;
                         region = DE;
                         regularMarketChange = "0.8600006";
                         regularMarketChangePercent = "1.7169108";
                         regularMarketDayHigh = "51.3";
                         regularMarketDayLow = "49.555";
                         regularMarketDayRange = "49.555 - 51.3";
                         regularMarketOpen = "49.61";
                         regularMarketPreviousClose = "50.09";
                         regularMarketPrice = "50.95";
                         regularMarketTime = 1611852139;
                         regularMarketVolume = 3877747;
                         revenue = 42155000000;
                         sharesOutstanding = 982424000;
                         shortName = "BAYER AG NA O.N.";
                         sourceInterval = 15;
                         symbol = "BAYN.DE";
                         targetPriceHigh = 140;
                         targetPriceLow = 100;
                         targetPriceMean = "119.69";
                         targetPriceMedian = 123;
                         totalCash = 15539999700;
                         tradeable = 0;
                         trailingAnnualDividendRate = "2.8";
                         trailingAnnualDividendYield = "0.05589938";
                         triggerable = 0;
                         twoHundredDayAverage = "51.366375";
                         twoHundredDayAverageChange = "-0.4163742";
                         twoHundredDayAverageChangePercent = "-0.008105968";
                     },
                     {
                         ask = "33.64";
                         askSize = 81;
                         averageDailyVolume10Day = 5022062;
                         averageDailyVolume3Month = 5149932;
                         beta = "1.541368";
                         bid = "33.61";
                         bidSize = 4279;
                         bookValue = "7.857";
                         components = (
                             "^GDAXI"
                         );
                         currency = EUR;
                         dividendRate = "0.22";
                         dividendYield = "0.68";
                         dividendsPerShare = "0.27";
                         earningsTimestamp = 1612402200;
                         earningsTimestampEnd = 1612402200;
                         earningsTimestampStart = 1612402200;
                         ebitda = 1820999936;
                         epsForward = "1.13";
                         epsTrailingTwelveMonths = "0.257";
                         esgPopulated = 0;
                         exDividendDate = 1614297600;
                         exchange = GER;
                         exchangeDataDelayedBy = 0;
                         exchangeTimezoneName = "Europe/Berlin";
                         exchangeTimezoneShortName = CET;
                         fiftyDayAverage = "32.190453";
                         fiftyDayAverageChange = "1.4095459";
                         fiftyDayAverageChangePercent = "0.0437877";
                         fiftyTwoWeekHigh = "35.92";
                         fiftyTwoWeekHighChange = "-2.3199997";
                         fiftyTwoWeekHighChangePercent = "-0.064587966";
                         fiftyTwoWeekLow = "10.132";
                         fiftyTwoWeekLowChange = "23.467999";
                         fiftyTwoWeekLowChangePercent = "2.3162258";
                         fiftyTwoWeekRange = "10.132 - 35.92";
                         firstTradeDateMilliseconds = 952934400000;
                         floatShares = 1300669746;
                         forwardPE = "29.734512";
                         fullExchangeName = XETRA;
                         gmtOffSetMilliseconds = 3600000;
                         heldPercentInsiders = 0;
                         heldPercentInstitutions = "54.235";
                         language = "de-DE";
                         longName = "Infineon Technologies AG";
                         market = "de_market";
                         marketCap = 43702509568;
                         marketState = POSTPOST;
                         messageBoardId = "finmb_105448_lang_de";
                         priceHint = 2;
                         priceToBook = "4.276441";
                         priceToSales = "5.1012616";
                         quoteSourceName = "Delayed Quote";
                         quoteSummary = {
                             earnings = {
                                 earningsChart = {
                                     currentQuarterEstimateDate = "4Q";
                                     currentQuarterEstimateYear = 2018;
                                     earningsDate = (
                                         1612396800
                                     );
                                     quarterly = (
                                         {
                                             actual = "0.2";
                                             date = "4Q2017";
                                             estimate = "0.19";
                                         },
                                         {
                                             actual = "0.26";
                                             date = "1Q2018";
                                             estimate = "0.21";
                                         },
                                         {
                                             actual = "0.24";
                                             date = "2Q2018";
                                             estimate = "0.23";
                                         },
                                         {
                                             actual = "0.28";
                                             date = "3Q2018";
                                             estimate = "0.27";
                                         }
                                     );
                                 };
                                 financialCurrency = EUR;
                                 financialsChart = {
                                     quarterly = (
                                         {
                                             date = "4Q2019";
                                             earnings = 209000000;
                                             revenue = 1917000000;
                                         },
                                         {
                                             date = "1Q2020";
                                             earnings = 178000000;
                                             revenue = 1986000000;
                                         },
                                         {
                                             date = "2Q2020";
                                             earnings = -128000000;
                                             revenue = 2174000000;
                                         },
                                         {
                                             date = "3Q2020";
                                             earnings = 109000000;
                                             revenue = 2490000000;
                                         }
                                     );
                                     yearly = (
                                         {
                                             date = 2017;
                                             earnings = 790000000;
                                             revenue = 7063000000;
                                         },
                                         {
                                             date = 2018;
                                             earnings = 1075000000;
                                             revenue = 7599000000;
                                         },
                                         {
                                             date = 2019;
                                             earnings = 870000000;
                                             revenue = 8029000000;
                                         },
                                         {
                                             date = 2020;
                                             earnings = 368000000;
                                             revenue = 8567000000;
                                         }
                                     );
                                 };
                                 maxAge = 86400;
                             };
                         };
                         quoteType = EQUITY;
                         region = DE;
                         regularMarketChange = "1.2399979";
                         regularMarketChangePercent = "3.8318846";
                         regularMarketDayHigh = "33.795";
                         regularMarketDayLow = "31.03";
                         regularMarketDayRange = "31.03 - 33.795";
                         regularMarketOpen = "31.56";
                         regularMarketPreviousClose = "32.36";
                         regularMarketPrice = "33.6";
                         regularMarketTime = 1611851884;
                         regularMarketVolume = 10318360;
                         revenue = 8567000100;
                         sharesOutstanding = 1299920000;
                         shortName = "INFINEON TECH.AG NA O.N.";
                         sourceInterval = 15;
                         symbol = "IFX.DE";
                         targetPriceHigh = 29;
                         targetPriceLow = 17;
                         targetPriceMean = "23.2";
                         targetPriceMedian = "23.25";
                         totalCash = 3227000060;
                         tradeable = 0;
                         trailingAnnualDividendRate = "0.22";
                         trailingAnnualDividendYield = "0.0067985165";
                         trailingPE = "130.73929";
                         triggerable = 0;
                         twoHundredDayAverage = "26.562174";
                         twoHundredDayAverageChange = "7.0378246";
                         twoHundredDayAverageChangePercent = "0.26495665";
                     }
                 )
     */
    class func fetchQuotes(symbols: Array<String>!, completionHandler handler: @escaping (Array<Dictionary<String, Any>>?, Error?) -> Void) -> Void {
        let dataTask = URLSession.shared.dataTask(with: createRequest(forEndpoint: ENDPOINT_QUOTES,
                                                                      parameter: [
                                                                        URLQueryItem(name: "region", value: "DE"),
                                                                        URLQueryItem(name: "symbols", value: symbols.joined(separator: ","))
                                                                      ])) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject["quoteResponse.result"] as? Array<Dictionary<String, Any>>, error)
            }
        }
        dataTask.resume()
    }


    /**
     Fetches data in Historical Data section for the specified `symbol` from the Yahoo Finance API at RapidAPI.com.

     This method should not be used very frequently. Due to the amount of data it takes much time the data rae fetched. So usually use this method
     for an initial request to fill your data base.
     - Parameters:
        - symbol:   A symbol that comforms to the Yahoo value identifier
        - handler:  A completion handler that is called after the requested result is returned from the API.
                    This completion handler function takes two arguments and has no return.
                    The first argument is an Array of Dictionary for each symbol with the structure listed below.
                    The second argument is an Error object that takes any error information. If is not `nil` the first argument is undefined.

                 [
                    "firstTradeDate": 850723200,
                    "isPending": 0,
                    "eventsData": (
                        {
                            amount = "2.8";
                            data = "2.8";
                            date = 1588143600;
                            type = DIVIDEND;
                        }
                    ),
                    "id": ,
                    "timeZone": {
                        gmtOffset = 3600;
                    },
                    "prices": (
                        {
                            adjclose = "50.95000076293945";
                            close = "50.95000076293945";
                            date = 1611852139;
                            high = "51.29999923706055";
                            low = "49.55500030517578";
                            open = "49.61000061035156";
                            volume = 3877747;
                        },
                        ...
                    )
                 ]
     */
    class func fetchHistoricalData(symbol: String!, completionHandler handler:  @escaping (Dictionary<String, Any>?, Error?) -> Void) -> Void {
        let dataTask = URLSession.shared.dataTask(with: createRequest(forEndpoint: ENDPOINT_HISTORICAL_DATA,
                                                                      parameter: [
                                                                        URLQueryItem(name: "region", value: "DE"),
                                                                        URLQueryItem(name: "symbol", value: symbol)
                                                                      ])) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject, error)
            }
        }
        dataTask.resume()
    }

}
