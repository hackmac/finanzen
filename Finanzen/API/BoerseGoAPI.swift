//
//  BoerseGoAPI.swift
//  Finanzen
//
//  Created by henrik on 06.02.21.
//

import Cocoa

class BoerseGoAPI: NSObject {

    private static let BASE_URL = "https://"
    private static let ENDPOINT_AUTOCOMPLETE = "https://api.boerse-go.de/instrument/"
    private static let ENDPOINT_CHARTING = "https://api.boerse-go.de/v1/services/charting/d/q"

    // MARK: - Such-API Beispiel-URLs

    private static let suchAPI = "https://api.boerse-go.de/instrument/?client_id=gmt&limit=5&page=1&search=deutsche &select=name,fullname,slug,status,assetClass[name,identifier],category,issuer[name],[autoQuotation,autoQuotations,quotations][delayed,unit[iso,name],exchange[name,fullname],subscription,instrumentName,defaultQuoteType,precision,quote[time,value,open,high,low,prevClose,change,changePerc,allowedGroups.realtime],bid[time,value,open,high,low,prevClose,change,changePerc,allowedGroups.realtime],ask[time,value,open,high,low,prevClose,change,changePerc,allowedGroups.realtime],last[time,value,open,high,low,prevClose,change,changePerc,allowedGroups.realtime]],components,listComponentOf.position,componentOf,identifiers[wkn,isin,deTicker],underlying,country[iso2,regions],product[issuer,eusipa2]"
    private static let suchAPIResponse = (
        [
            "id": 118687,
            "assetClass": [
                "id": 1,
                "name": "Aktie"
            ],
            "name": "Deutsche Bank AG",
            "fullname": "Deutsche Bank AG Namens-Aktien o.N.",
            "identifiers": [
                "isin": "DE0005140008",
                "wkn": "514000"
            ]
        ],
        [
            "fullname": "Deutsche Post AG Namens-Aktien o.N.",
            "identifiers": [
                "isin": "DE0005552004",
                "wkn": "555200"
            ],
            "name": "Deutsche Post AG",
            "id": 122006,
            "assetClass": [
                "id": 1,
                "name": "Aktie"
            ]
        ]
    )

    enum AssetClass: Int {
        case Stocks = 1, Indices, Currency, Commodities, Fonds, unknown, Futures, Derivates, Zinsen

        static func > (left: AssetClass, right: AssetClass) -> Bool {
            return left.rawValue > right.rawValue
        }
        static func < (left: AssetClass, right: AssetClass) -> Bool {
            return left.rawValue < right.rawValue
        }
    }

    private static let suchAPIFontDetail = "https://www.godmode-trader.de/api/v1/instrument/.json?do=list&page=1&limit=30&filter=assetClass.id=1,5&search=deutsche&_t=57545f1c8ae80fef1995c05d89414d0e&attributes=slug,assetClass,identifiers[isin,wkn],refQuotation[currency.symbol,quote.precision]&facets=assetClass.id&portal=gmt"

    /*
     eid=4  => XETRA
     eid=37 => Tradegate

     iid => Instrument ID (interne Wertpapier ID)
     qs => last | ask | bid
     */
    private static let chartAPI = "https://api.boerse-go.de/v1/services/charting/d/q?iid=122121&res=86400&qs=last&eid=4"

    private static let metaAPI = "https://api.boerse-go.de/v1/services/charting/d/meta?id=122121"

    private static let historicQuotesAPI = "https://api.boerse-go.de/v1/services/charting/d/q?iid=122121&eid=22&qs=bid&res=86400&locale=de&portal=grid&_t=57545f1c8ae80fef1995c05d89414d0e"

    private static let profileAPI = "https://api.boerse-go.de/product-search/122121?locale=de&client_id=grid&access_token_not_available=&action=instrumentProfile"


    // MARK: -


    private class func createRequest(forEndpoint endpoint: String, parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: BASE_URL)!
        urlComponents.path = "/" + endpoint
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        //request.allHTTPHeaderFields = headers

        return request
    }


    private class func createRequest(forUrl url: String, parameter queryItems: [URLQueryItem]) -> URLRequest {
        var urlComponents = URLComponents(string: url)!
        urlComponents.queryItems = queryItems
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 2.0 * 60.0)
        request.httpMethod = "GET"
        //request.allHTTPHeaderFields = headers

        return request
    }
    

    class func fetchAutocomplete(value: String!, allowedAssetClasses: [AssetClass] = [.Stocks, .Fonds],
                                 completionHandler handler: @escaping (Array<Dictionary<String, Any>>?, Error?) -> Void) -> Void {
        var parameter = [
            /* id for 'Godmode Trader' */
            URLQueryItem(name: "client_id", value: "gmt"),
            /* properties for paging */
            URLQueryItem(name: "limit", value: 50.description),   // default: 10
            //URLQueryItem(name: "page", value: 1.description),
            /* property that defines what to select for the resulting response */
            URLQueryItem(name: "select", value: "name,fullname,assetClass[name],identifiers[wkn,isin]"),
            /* the search term */
            URLQueryItem(name: "search", value: value)
        ]
        if 0 < allowedAssetClasses.count {
            let assetClassIDs = allowedAssetClasses.sorted(by: <).map({val in val.rawValue.description}).joined(separator: ",")
            /* filtering */
            parameter.append(
                URLQueryItem(name: "filter", value: "assetClass.id=\(assetClassIDs)")
            )
        }

        let request = createRequest(forUrl: ENDPOINT_AUTOCOMPLETE, parameter: parameter)
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject["data"] as? Array<Dictionary<String, Any>>, error)
            } else {
                print(httpResponse)
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }


    class func fetchHistoricQuotes(id: UInt, handler: @escaping (Dictionary<String, Any>?, Error?) -> Void) -> Void {
        let request = createRequest(forUrl: ENDPOINT_CHARTING,   // https://api.boerse-go.de/v1/services/charting/d/q?iid=122121&res=86400&qs=last&eid=4
                                    parameter: [
                                        /* internal id for security */
                                        URLQueryItem(name: "iid", value: id.description),
                                        URLQueryItem(name: "res", value: 86400.description),
                                        URLQueryItem(name: "qs", value: "last"),
                                        /* exchange id */
                                        URLQueryItem(name: "eid", value: 4.description) // for Xetra
                                    ])
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if (nil != error) {
                handler(nil, error)
                return
            }

            let httpResponse = response as! HTTPURLResponse
            if (200 == httpResponse.statusCode) {
                let jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String, Any>
                handler(jsonObject["data"] as? Dictionary<String, Any>, error)
            } else {
                print(httpResponse)
                let error = NSError(domain: NSCocoaErrorDomain, code: httpResponse.statusCode, userInfo: [
                    NSLocalizedFailureReasonErrorKey : HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                ])
                handler(nil, error)
            }
        }
        dataTask.resume()
    }

}
